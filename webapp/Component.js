// define a root UI component that exposes the main view
jQuery.sap.declare("STELO_TS03E01.Component");
jQuery.sap.require("sap.ui.core.UIComponent");
jQuery.sap.require("sap.ui.core.routing.History");
jQuery.sap.require("sap.m.routing.RouteMatchedHandler");

sap.ui.core.UIComponent.extend("STELO_TS03E01.Component", {
	metadata: {
		"manifest": "json",
		"formType": "TS03",
		"formDescription": "TS03-Manager Timesheet",
		"TemplateType": "E",
		"SteloVersion": "01",
		"stelolauncher": "",
		"formlauncher": "X",
		"hrlauncher": "",
		"SteloContainer": "",
		"LauncherRequired": "",
		"LauncherObject": "",
		"name": "Stelo Dynamic Template",
		"version": "1.1.0-SNAPSHOT",
		"containerType": "panel",
		"titleHeader": "",
		"objectAttribute1": "",
		"objectAttribute2": "",
		"objectAttribute3": "",
		"detailContainerType": "",
		"showHeader": "",
		"library": "STELO_TS03E01",
		"includes": ["css/fullScreenStyles.css"],
		"dependencies": {
			"libs": ["sap.m", "sap.ui.layout"],
			"components": []
		}
	},
	/**
	 * Initialize the application
	 *
	 * @returns {sap.ui.core.Control} the content
	 */
	createContent: function () {
		var oViewData = {
			component: this
		};

		return sap.ui.view({
			viewName: "STELO_TS03E01.view.Main",
			type: sap.ui.core.mvc.ViewType.XML,
			viewData: oViewData
		});
	},

	init: function () {
		sap.ui.core.UIComponent.prototype.init.apply(this, arguments);
		// always use absolute paths relative to our own component
		// (relative paths will fail if running in the Fiori Launchpad)
		var sRootPath = jQuery.sap.getModulePath("STELO_TS03E01");
		this._routeMatchedHandler = new sap.m.routing.RouteMatchedHandler(this.getRouter());
		// initialize router and navigate to the first page
		this.getRouter().initialize();
	},

	destroy: function () {
		// call the base component's destroy function
		sap.ui.core.UIComponent.prototype.destroy.apply(this, arguments);
		this._routeMatchedHandler.destroy();
	},

	exit: function () {
		this._routeMatchedHandler.destroy();
	}
});