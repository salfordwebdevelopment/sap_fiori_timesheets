sap.ui.controller("STELO_TS03E01.controller.Main", {

	onInit: function () {
		if (sap.ui.Device.support.touch === false) {
			this.getView().addStyleClass("sapUiSizeCompact");
		}
		//Declare global model
		var globalModel = new sap.ui.model.json.JSONModel();
		var globalObj = {};
		globalObj.draftText = "";
		this.getOwnerComponent().setModel(globalModel, "globalModel");
		globalModel.setData(globalObj);
	}
});