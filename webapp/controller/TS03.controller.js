jQuery.sap.require("sap.ui.core.mvc.Controller");
jQuery.sap.require("sap.ui.core.util.MockServer");
jQuery.sap.require("sap.ui.core.format.DateFormat");
jQuery.sap.require("sap.m.MessagePopover");
jQuery.sap.require("sap.m.MessageBox");

sap.ui.core.mvc.Controller.extend("STELO_TS03E01.controller.TS03", {
	//------------------------------------------------------------------------------------------------------------------//
	//Start of Form Related Initialisation Stubs
	//------------------------------------------------------------------------------------------------------------------//
	onInit: function() {
		this._fieldArray = [];
		this.subformsPlaced = [];
		this._validationSubforms = {};
		this._oView = this.getView();
		this._oComponent = sap.ui.component(sap.ui.core.Component.getOwnerIdFor(this._oView));
		this._oRouter = this.getOwnerComponent().getRouter();
		this._oRouter.attachRoutePatternMatched(this._onRoutePatternMatched, this);
		this._globalAttUploadCounter = 0;
		this._SubmissionFlag = false;
		this._oFormData = {};
	},

	_onRoutePatternMatched: function(oEvent) {
		//Data Declarations
		var sURL, oModel, oView;
		var sURI = '/sap/opu/odata/STELO/APP_SERVER_SRV/';
		var oModelF41 = new sap.ui.model.xml.XMLModel();
		var oModelF42 = new sap.ui.model.xml.XMLModel();
		var oModelF43 = new sap.ui.model.xml.XMLModel();
		var oModelF44 = new sap.ui.model.xml.XMLModel();
		var oModelF45 = new sap.ui.model.xml.XMLModel();
		var oModelAtt = new sap.ui.model.json.JSONModel();
		var oModel1 = new sap.ui.model.odata.ODataModel(sURI, true);
		var that = this;
		oView = this.getView();
		//Setup blank model to handle attachments
		this.getView().setModel(oModelAtt, "attachmentsData");

		if (oEvent.getParameter("name") === "formContainer" || oEvent.getParameter("name") === "landingView" || oEvent.getParameter("name") ===
			"directFormView") {
			//Read parameter from URL
			var empId;
			if (!oEvent.getParameter("arguments").empId) {
				empId = "00000000";
			} else {
				empId = atob(decodeURIComponent(oEvent.getParameter("arguments").empId));
			}
			//Build Entity URL
			var sEntity =
				"/AppDataOutSet(IM_DOCUMENT='" + empId +
				"',IM_ID='0000',IM_ID_VAR='0000',IM_FTYPE='TS03',IM_CCODE='',IM_FVER='01',IM_FLANG='E',IM_FILLABLE='X',IM_CALLER='C')";
			this._triggeredBy = oEvent.getParameter("name");

		} else if (oEvent.getParameter("name") === "formContainerDirect") {
			var formId = decodeURIComponent(oEvent.getParameter("arguments").formId);
			var formIdVar = decodeURIComponent(oEvent.getParameter("arguments").formIdVar);
			var triggeredBy = decodeURIComponent(oEvent.getParameter("arguments").triggeredBy);
			var formVer = decodeURIComponent(oEvent.getParameter("arguments").formVer);
			var formLang = decodeURIComponent(oEvent.getParameter("arguments").formLang);
			this._triggeredBy = triggeredBy;

			if (triggeredBy === "History" || triggeredBy === "Dashboard") {
				if (triggeredBy === "Dashboard") {
					var caller = "D";
				} else {
					caller = "H";
				}
				sEntity =
					"/AppDataOutSet(IM_DOCUMENT='00000000',IM_ID='" + formId + "',IM_ID_VAR='" + formIdVar +
					"',IM_FTYPE='TS03',IM_CCODE='ACL',IM_FVER='" + formVer + "',IM_FLANG='" + formLang + "',IM_FILLABLE='',IM_CALLER='" +
					caller + "')";
			} else {
				sEntity =
					"/AppDataOutSet(IM_DOCUMENT='00000000',IM_ID='" + formId + "',IM_ID_VAR='" + formIdVar +
					"',IM_FTYPE='TS03',IM_CCODE='ACL',IM_FVER='" + formVer + "',IM_FLANG='" + formLang + "',IM_FILLABLE='X',IM_CALLER='I')";
			}
			//Set navigation bar visible
			oView.byId("backButton").setText("Back" + " to " + triggeredBy);
		}

		if (oEvent.getParameter("name") === "formContainer" || oEvent.getParameter("name") === "formContainerDirect" || oEvent.getParameter(
				"name") === "landingView" || oEvent.getParameter("name") === "directFormView") {
			//Common Stub for navigation from different views
			this._oDialog = this.getView().byId("BusyDialog");
			this._oDialog.setText("Prepopulating from server");
			this._oDialog.open();

			var oModelCSRF = new sap.ui.model.odata.ODataModel(sURI, true);
			this._csrfToken = oModelCSRF.getSecurityToken();

			//Read data from backend via Gateway
			oModel1.read(sEntity, null, null, true, function(oData, oResponse) {
				that.ResponseData = oResponse.data;
				oModelF41.setXML(oResponse.data.EX_XML);
				oModelF42.setXML(oResponse.data.EX_F4_XML);

				//Read owner and status
				that._formOwner = oResponse.data.EX_USER;
				that._formStatus = oResponse.data.EX_FSTATUS;

				if (oResponse.data.EX_AUDIT_TRAIL) { //If Audit trail exists
					oModelF45.setXML(oResponse.data.EX_AUDIT_TRAIL);
				}

				if (oResponse.data.EX_ACTIONS_XML) { //If Actions exists
					oModelF43.setXML(oResponse.data.EX_ACTIONS_XML);
				}

				if (oResponse.data.EX_ATTACHMENTS) { //If Attachments Exists
					oModelF44.setXML(oResponse.data.EX_ATTACHMENTS);
					oView.setModel(oModelF44, "attachmentsModel");
					//Initialise Attachments view
					that.initialiseAttachments();
				}

				if (oResponse.data.EX_NO_ATTS) { //If No Attachments are required - then hide attachments button
					oView.byId("attachmentAnchor").setVisible(false);
				}

				oView.setModel(oModelF42, "F4Model");
				oView.setModel(oModelF43, "flmactionModel");
				that.setupPrepopModel(oView, oModelF41);
				that.buildFieldArray();

				//Lock down form fields
				if (triggeredBy === "History" || triggeredBy === "Dashboard") {
					if (triggeredBy === "Dashboard") {
						//Hide back button
						that.getView().byId("backButton").setVisible(false);
						oView.setModel(oModelF45, "auditTrail");
						that.addAuditTrailSeparators();
						that.getView().byId("auditTrail").setVisible(true);
						that.getView().byId("auditTrailPanel").setVisible(true);
					}
					//Disable comments field for history & dashboard
					if (that.getView().byId("TXT_NEW_COMMENTS")) {
						that.getView().byId("TXT_NEW_COMMENTS").setEnabled(false);
					}

				} else {
					//Setup action buttons in header only for interactive forms
					if (oResponse.data.EX_ACTIONS_XML) { //If Actions Exist
						that.setupHeaderActions();
					}
					oView.byId("cancelAction").setVisible(true);
				}
				that._oDialog.close();
			});
		}
	},

	setupHeaderActions: function() {
		var oView = this.getView();
		var actionModel = oView.getModel("flmactionModel");
		var bufferModel, actionLabel, actionKey, actionID;
		this.removeHeaderActions();
		for (var i = 1; i < actionModel.oData.childNodes[0].childNodes.length; i++) {
			bufferModel = new sap.ui.model.xml.XMLModel();
			bufferModel.setData(actionModel.oData.childNodes[0].childNodes[i]);
			bufferModel.setXML(bufferModel.getXML());
			actionLabel = bufferModel.getProperty("/label");
			//Remove any spaces from the label
			actionID = actionLabel.replace(/ /g, '');
			actionKey = bufferModel.getProperty("/data");

			var actionFragId = actionID + jQuery.now().toString(); //Generate dynamic id
			var actionButtonFragment = sap.ui.xmlfragment(actionFragId, "STELO_TS03E01.view.actionButton",
				this);
			var actionButton = sap.ui.core.Fragment.byId(actionFragId, "actionButton");
			actionButton.setText(actionLabel);
			actionButton.setIcon("sap-icon://action");
			actionButton.data("actionKey", actionKey);
			oView.byId("actionBar").insertContent(actionButtonFragment, i + 1);
		}
	},

	removeHeaderActions: function() {
		var oView = this.getView();
		var toolBarActions = oView.byId("actionBar").getContent();
		for (var i = 2; i < toolBarActions.length - 1; i++) {
			oView.byId("actionBar").removeContent(toolBarActions[i]);
		}
	},

	handleBack: function() {
		this.unlockForm(); //Unlock form - KB20160331++
		var oHistory = sap.ui.core.routing.History.getInstance();
		var sPreviousHash = oHistory.getPreviousHash();
		var triggeredBy = this._triggeredBy;
		this.resetFields();
		if (triggeredBy === "Inbox" || triggeredBy === "Draft" || triggeredBy === "History" || triggeredBy ===
			"landingView") {
			window.history.go(-1);
		} else {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo(this._oRouter._oRoutes.landingView._oConfig.target, true);
		}
	},

	unlockForm: function() { //new function KB20160331++
		var oDialog = this._oView.byId("BusyDialog");
		oDialog.setText("Unlocking document");
		oDialog.open();
		var sURI = '/sap/opu/odata/STELO/APP_SERVER_SRV/';
		var oModel = new sap.ui.model.odata.ODataModel(sURI, true);
		var oView = this.getView();
		var flmReturn = oView.getModel("prepopModel").getProperty("/FLM_RETURN").split("+");
		var cmsDoc = flmReturn[1];
		var sEntity = "/UnlockFormSet(CMS_DOC='" + cmsDoc + "')";

		oModel.remove(sEntity, true, function(oData, oResponse) {
			oDialog.close();
		}, function() {
			oDialog.close();
			sap.m.MessageToast.show("Unlock failed");
		});
	},
	//------------------------------------------------------------------------------------------------------------------//
	//End of Form Related Initialisation Stubs
	//------------------------------------------------------------------------------------------------------------------//	

	//------------------------------------------------------------------------------------------------------------------//
	//Start of Form Related Validations/Specific Stubs
	//------------------------------------------------------------------------------------------------------------------//
	setupPrepopModel: function(oView, oModelF41) {
		oView.setModel(oModelF41, "prepopModel");
		this._formID = oModelF41.getProperty("/FLM_RETURN").substr(16, 10);
		this._formVariant = oModelF41.getProperty("/FLM_RETURN").substr(27, 4);
		this._flmreturn = oModelF41.getProperty("/FLM_RETURN"); //Store FLM Return Globally
	},

	buildFieldArray: function() {
		// debugger;
		var noNodes = this.getView().getModel("prepopModel").oData.childNodes[0].childNodes.length;
		for (var i = 0; i < noNodes; i++) {
			var nodeName = this.getView().getModel("prepopModel").oData.childNodes[0].childNodes[i].nodeName;
			if (nodeName.substr(0, 2) !== "SF") {
				//Add to field array
				var fieldObject = {};
				fieldObject.name = nodeName;
				fieldObject.subformName = "ROOT";
				var sectionId = $("#" + nodeName).closest("section").attr("id");
				if (sectionId) {
					fieldObject.subformPlaced = sectionId.split("--")[1];
				} //If section exists->field is placed in view container

				fieldObject.value = this.getView().getModel("prepopModel").oData.childNodes[0].childNodes[i].innerHTML;
				this._fieldArray.push(fieldObject);
			} else {
				this.getChildren(this.getView().getModel("prepopModel").oData.childNodes[0].childNodes[i]);
			}
		}
	},

	//Find all children of this parent node and update the field array 
	getChildren: function(oParent) {
		for (var k = 0; k < oParent.childNodes.length; k++) {
			var nodeName = oParent.childNodes[k].nodeName;
			var fieldObject = {};
			fieldObject.name = nodeName;
			if (nodeName.substr(0, 2) !== "SF") {
				//Add to field array
				fieldObject.name = nodeName;
				fieldObject.subformName = oParent.nodeName;
				var sectionId = $("#" + nodeName).closest("section").attr("id");
				if (sectionId) {
					fieldObject.subformPlaced = sectionId.split("--")[1];
				} //If section exists->field is placed in view container
				fieldObject.value = oParent.childNodes[k].innerHTML;
				this._fieldArray.push(fieldObject);
			} else {
				this.getChildren(oParent.childNodes[k]);
			}
		}
	},

	handleHelpText: function(oEvent) {
		// TODO: 
	},

	addComments: function(oEvent) {
		var oModel = this.getView().getModel("prepopModel");
		var objCmt = oModel.getObject("/SF_CMTS/SF_CMT/0/");
		if (objCmt) { //Check if SF_CMTS exist
			var mainCmtElm;
			var oView = this.getView();
			var iconURI = oView.byId("TXT_NEW_COMMENTS").getIcon();
			var userName = this._formOwner;
			var fstatus = this._formStatus;

			//get a human readable date
			var oFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
				style: "medium"
			});
			var oDate = new Date();
			var sDate = oFormat.format(oDate);

			//now get the value added
			var sValue = oEvent.getParameter("value");

			if (oModel.getObject("/SF_CMTS").childNodes.length === 1 && oModel.getObject("/SF_CMTS/SF_CMT/0").childNodes[0].textContent === "") {
				oModel.getObject("/SF_CMTS/SF_CMT/0").childNodes[0].textContent = sDate;
				oModel.getObject("/SF_CMTS/SF_CMT/0").childNodes[1].textContent = fstatus;
				oModel.getObject("/SF_CMTS/SF_CMT/0").childNodes[2].textContent = sValue;
				oModel.getObject("/SF_CMTS/SF_CMT/0").childNodes[3].textContent = userName;
				oModel.getObject("/SF_CMTS/SF_CMT/0").childNodes[4].textContent = iconURI;
			} else {

				//now create our new clone
				mainCmtElm = objCmt.cloneNode(true);
				mainCmtElm.childNodes[0].textContent = sDate;
				mainCmtElm.childNodes[1].textContent = fstatus;
				mainCmtElm.childNodes[2].textContent = sValue;
				mainCmtElm.childNodes[3].textContent = userName;
				mainCmtElm.childNodes[4].textContent = iconURI;
				//Now add the element
				var cmtsMain = oModel.getObject("/SF_CMTS");
				//cmtsMain.appendChild(mainCmtElm);
				oModel.getObject("/SF_CMTS").appendChild(mainCmtElm);
			}
			//update the model
			oModel.refresh();
			//finally make the section visible
			oView.byId("TXT_PREV_COMMENTS").setVisible(true);
		}
	},
	handleCancel: function(oEvent) {
		try {
			this.handleBack();
		} catch (error) {
			// TODO: 
		}
	},

	handleChange: function(oEvent) {
		var oView = this.getView();
		var elemId = oEvent.getSource().getId().split("--")[1];

		switch (oEvent.getSource().getMetadata().getName()) {
			case "sap.m.Select":
				//Remove highlighting
				oEvent.getSource().removeStyleClass("errorHighlight");
				break;
			default:
				oEvent.getSource().setShowValueStateMessage(false); //Hide error text
				oEvent.getSource().setValueState("None"); //remove field highlighting if any
				break;
		}

	},

	radioFormatter: function(data) {
		if (data === "" || data === null) { //Do not select anything
			data = -1;
		}
		var data = parseInt(data);
		return data;
	},
	checkFormatter: function(data) {
		if (data === "" || data === "false" || data === null) { //Do not select anything
			data = false;
		} else {
			data = true;
		}
		return data;
	},
	handleCheckSelect: function(oEvent) {
		$("#" + oEvent.getSource().getId() + "-CbBg").removeClass("errorHighlight"); //remove field highlighting if any
		var selectedObjectPath = oEvent.getSource().data("name");
		this.getView().getModel("prepopModel").setProperty(selectedObjectPath, oEvent.getParameter("selected"));
	},
	handleSwitchSelect: function(oEvent) {
		$("#" + oEvent.getSource().getId() + "-switch").removeClass("errorHighlight"); //remove field highlighting if any
		var selectedObjectPath = oEvent.getSource().data("name");
		this.getView().getModel("prepopModel").setProperty(selectedObjectPath, oEvent.getParameter("state"));
	},
	handleDateChange: function(oEvent) {
		oEvent.getSource().setShowValueStateMessage(false); //Hide error text
		oEvent.getSource().setValueState("None"); //remove field highlighting if any
	},
	handleRadioSelect: function(oEvent) {
		oEvent.getSource().setValueState("None"); //remove field highlighting if any
		var selectedObjectPath = oEvent.getSource().data("name");
		this.getView().getModel("prepopModel").setProperty(selectedObjectPath, oEvent.getParameter("selectedIndex"));
	},

	callDataService: function(funcname, imdata, operation) {
		var dfd = $.Deferred();
		var sURI = '/sap/opu/odata/STELO/UTILITIES_SRV/';
		var oModelStelo = new sap.ui.model.odata.ODataModel(sURI);
		var imCmsDoc = this._cmsdoc;
		var sEntity;

		if (operation === "CREATE") {
			sEntity = "/DataServiceSet";
			var dataServiceObject = {};
			dataServiceObject.IM_DATA = imdata;
			dataServiceObject.IM_FUNCTION = funcname;
			dataServiceObject.IM_APPTYPE = 'TS03';

			oModelStelo.create(sEntity, dataServiceObject, null, function(oData, oResponse) {
					if (oResponse.data.EX_DATA) {
						var exData = JSON.parse(oResponse.data.EX_DATA);
					}
					if (oResponse.data.EX_MESSAGE) {
						var exMessage = JSON.parse(oResponse.data.EX_MESSAGE);
					}

					if (exMessage === null || exMessage.type !== "E") {
						dfd.resolve(exData);
					} else {
						dfd.resolve("Error");
						var errMessage = exMessage.message;
						sap.m.MessageBox.show(
							errMessage, {
								icon: sap.m.MessageBox.Icon.ERROR,
								title: "Error calling dataservice " + funcname,
								actions: [sap.m.MessageBox.Action.OK]
							}
						);
					}

				},
				function(error) {

					if (error.response.statusCode === "503") {
						var errMessage = "Session has expired - please login after refreshing the page";
					} else {
						errMessage = error.response.body;
					}
					sap.m.MessageBox.show(
						errMessage, {
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "Error calling dataservice " + funcname,
							actions: [sap.m.MessageBox.Action.OK]
						}
					);
				});
		} else {
			sEntity = "/DataServiceSet(IM_APPTYPE='TS03',IM_FUNCTION='" + funcname + "',IM_DATA='" + imdata + "')";
			oModelStelo.read(sEntity, null, null, null, function(data, oResponse) {
					if (oResponse.data.EX_DATA) {
						var exData = oResponse.data.EX_DATA;
					}
					if (oResponse.data.EX_MESSAGE) {
						var exMessage = oResponse.data.EX_MESSAGE;
					}

					dfd.resolve(exData, exMessage);
				},
				function(error) {
					if (error.response.statusCode === "503") {
						var errMessage = "Session has expired - please login after refreshing the page";
					} else {
						errMessage = error.response.body;
					}
					sap.m.MessageBox.show(
						errMessage, {
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "Error calling dataservice " + funcname,
							actions: [sap.m.MessageBox.Action.OK]
						}
					);
				});
		}

		//Read available form types
		return dfd.promise();
	},
	//------------------------------------------------------------------------------------------------------------------//
	//End of Form Related Validations/Specific Stubs
	//------------------------------------------------------------------------------------------------------------------//

	//------------------------------------------------------------------------------------------------------------------//
	//Start of Form Submission Related Stubs
	//------------------------------------------------------------------------------------------------------------------//
	handleAction: function(oEvent) {
		// debugger;
		this._actionKey = oEvent.getSource().data("actionKey");
		if (this._actionKey !== 'D') {
			this.handleSubmit(oEvent); //Run validation before submission->Mandatory fields to be handled from backend/manually by the developer on the frontend
		} else {
			//Show dialog to capture draft description
			this.captureDraftDescription();
			// TODO: Handle for other statuses
		}
	},

	captureDraftDescription: function() {
		var that = this;

		var dialog = new sap.m.Dialog("draftDialog", {
			title: 'Draft Notes',
			type: 'Message',
			content: [
				new sap.m.TextArea('draftNotes', {
					liveChange: function(oEvent) {
						var sText = oEvent.getParameter('value');
						var parent = oEvent.getSource().getParent();
						parent.getBeginButton().setEnabled(sText.length > 0);
					},
					width: '100%',
					placeholder: 'Enter draft notes'
				})
			],
			beginButton: new sap.m.Button({
				text: 'Submit',
				enabled: false,
				press: function() {
					//Store draft text in global form data object
					that._oFormData.IM_DRAFT_TEXT = sap.ui.getCore().byId("draftNotes").getValue();
					//Call submission routine
					that.doFormSubmission();
					dialog.close();
				}
			}),
			endButton: new sap.m.Button({
				text: 'Cancel',
				press: function() {
					dialog.close();
				}
			}),
			afterClose: function() {
				dialog.destroy();
			}
		});
		dialog.open();
		if (window.draftText) { //If draft text exits
			dialog.getContent()[0].setValue(window.draftText);
			dialog.getBeginButton().setEnabled(true);
		}
	},

	submissionCompleteDialog: function(successMessage) {
		var that = this;
		var dialog = new sap.m.Dialog({
			title: 'Success',
			type: 'Message',
			content: new sap.m.Text({
				text: successMessage
			}),
			beginButton: new sap.m.Button({
				text: 'OK',
				press: function() {
					dialog.close();
					that.handleBack();
				}
			}),
			afterClose: function() {
				dialog.destroy();
			}
		});
		dialog.open();
	},

	handleSubmit: function(oEvent) {
		// debugger;
		var oView = this.getView();
		var that = this;
		//Carry out form validation before form submission
		//Loop through the form and validate
		var errCounter = 0;
		var elemValue, elemId;
		var formData = [];
		var localCounter = 0;
		this._validationSubforms = {}; //Reset the validation array object
		//Run validation
		for (var i = 0; i < this._fieldArray.length; i++) {
			var fieldElement = oView.byId(this._fieldArray[i].name);
			elemId = this._fieldArray[i].name;
			if (fieldElement) { //Check if field element exists in the view

				switch (fieldElement.getMetadata().getName()) {
					case "sap.m.Select":
						elemValue = fieldElement.getSelectedKey();
						if (elemValue === "" && fieldElement.data("required") === "true") {
							fieldElement.addStyleClass("errorHighlight");
							errCounter++;
						} else {
							fieldElement.removeStyleClass("errorHighlight");
						}
						break;
					case "sap.m.CheckBox":
						elemValue = fieldElement.getSelected();
						if (elemValue === false && fieldElement.data("required") === "true") {
							//fieldElement.addStyleClass("errorHighlight");
							$("#" + fieldElement.getId() + "-CbBg").addClass("errorHighlight");
							errCounter++;
						} else {
							//fieldElement.removeStyleClass("errorHighlight");
							$("#" + fieldElement.getId() + "-CbBg").removeClass("errorHighlight");
						}
						break;
					case "sap.m.Switch":
						elemValue = fieldElement.getState();
						if (elemValue === false && fieldElement.data("required") === "true") {
							//fieldElement.addStyleClass("errorHighlight");
							$("#" + fieldElement.getId() + "-switch").addClass("errorHighlight");
							errCounter++;
						} else {
							//fieldElement.removeStyleClass("errorHighlight");
							$("#" + fieldElement.getId() + "-switch").removeClass("errorHighlight");
						}
						break;
					case "sap.m.RadioButton":
						elemValue = fieldElement.getSelected();
						if (elemValue === false && fieldElement.data("required") === "true") {
							//fieldElement.addStyleClass("errorHighlight");
							$("#" + fieldElement.getId() + "-Button").addClass("errorHighlight");
							errCounter++;
						} else {
							//fieldElement.removeStyleClass("errorHighlight");
							$("#" + fieldElement.getId() + "-Button").removeClass("errorHighlight");
						}
						break;
					case "sap.m.RadioButtonGroup":
						elemValue = fieldElement.getSelectedIndex();
						if (elemValue === -1 && fieldElement.data("required") === "true") {
							fieldElement.setValueState("Error");
							errCounter++;
						} else {
							// TODO: 
						}
						break;
					case "sap.m.Input":
					case "sap.m.DatePicker":
					case "sap.m.TimePicker":
						elemValue = fieldElement.getValue();
						if (elemValue === "" && fieldElement.data("required") === "true") {
							fieldElement.setValueState("Error");
							fieldElement.setValueStateText("Mandatory field cannot be left empty");
							errCounter++;
						} else {
							// TODO: 
						}
						break;
				}
			}
			formData.push({
				name: elemId,
				value: elemValue
			});
		}

		if (errCounter === 0) {
			this._SubmissionFlag = true;
			that.doFormSubmission();
		} else {
			sap.m.MessageToast.show("Mandatory fields are empty");
		}
	},

	doFormSubmission: function() {
		if (this._globalAttUploadCounter === 0) {
			var formData;
			//Get CSRF Token and send data to backend
			var sURI = '/sap/opu/odata/STELO/APP_SERVER_SRV/';
			var oModel11 = new sap.ui.model.odata.ODataModel(sURI, true);
			var sEntity = "/AppDataInSet";
			var csrfToken = oModel11.getSecurityToken();
			var oView = this.getView();
			//Update FLM Return and FLM Action in the model
			oView.getModel("prepopModel").setProperty("/FLM_RETURN", this._actionKey + oView.getModel("prepopModel").getProperty("/FLM_RETURN").substr(
				1));
			oView.getModel("prepopModel").setProperty("/FLM_ACTION", this._actionKey);
			var formData = oView.getModel("prepopModel").getXML();
			this._oFormData.IM_DATA = formData;

			var that = this;
			that._oDialog.setText("Uploading form data to server");
			that._oDialog.open();

			setTimeout(function() {
				oModel11.create(sEntity, that._oFormData, null, function(oData, oResponse) {
					that._oDialog.close();
					//Read response data
					var responseArray = JSON.parse(oResponse.data.EX_DATA);
					if (responseArray.length > 0) {
						if (responseArray[0].type === "S") { //Successful posting
							that.submissionCompleteDialog(responseArray[0].message);
							//Hide anchor if made visible
							that.getView().byId("validationErrorAnchor").setVisible(false).setText("");
						} else { //Build show error message
							//Show validation anchor button
							that.getView().byId("validationErrorAnchor").setVisible(true).setText("Validation Error(" + responseArray.length + ")");
							that.handlePostValidationError(responseArray);
						}
					} else {
						that.getView().byId("validationErrorAnchor").setVisible(false).setText("");
						sap.m.MessageToast.show("No response received from server - contact system administrator");
					}

				}, function() {
					//	oDialog.close();
					sap.m.MessageToast.show("Create failed");
				});
			}, 1000);

		} else {
			sap.m.MessageToast.show("Attachment upload is in progress - please wait");
		}
	},

	resetFields: function() {
		try {
			$("div").scrollTop(0); //Scroll container back to top
			//Clear back button text
			this.getView().byId("backButton").setText("Back");
			this.getView().byId("cancelAction").setVisible(false);
			this.getView().byId("attachmentAnchor").setVisible(true);
			//Updates
			this._triggeredBy = "";
			//Flush the model data
			this.getView().getModel("prepopModel").setData("");
			this.getView().getModel("F4Model").setData("");
			if (this.getView().getModel("flmactionModel")) {
				this.getView().getModel("flmactionModel").setData("");
			}
			if (this.getView().getModel("attachmentsModel")) {
				this.getView().getModel("attachmentsModel").setData("");
			}
			this.getView().byId("attachmentAnchor").setText("Attachments");

			if (sap.ui.core.Fragment.byId("attachD", "UploadCollection")) {
				sap.ui.core.Fragment.byId("attachD", "UploadCollection").setNumberOfAttachmentsText("Attachments");
			}
			this._formOwner = "";
			this._formStatus = "";
			this._globalAttUploadCounter = 0;
			this._validationSubforms = {}; //Reset the validation array object
			//Remove header actions
			this.removeHeaderActions();
			var oView = this.getView();
			//Remove field highlighting
			for (var i = 0; i < this._fieldArray.length; i++) {
				var fieldElement = oView.byId(this._fieldArray[i].name);
				if (fieldElement) {
					if (fieldElement.getMetadata().getName() === "sap.m.Select") {
						fieldElement.removeStyleClass("errorHighlight");
					} else if (fieldElement.getMetadata().getName() === "sap.m.Input" || fieldElement.getMetadata().getName() === "sap.m.DatePicker") {
						fieldElement.setShowValueStateMessage(false); //Hide error text
						fieldElement.setValueState("None"); //remove field highlighting if any
					} else if (fieldElement.getMetadata().getName() === "sap.m.RadioButton") {
						$("#" + fieldElement.getId() + "-Button").removeClass("errorHighlight");

					} else if (fieldElement.getMetadata().getName() === "sap.m.CheckBox") {
						$("#" + fieldElement.getId() + "-CbBg").removeClass("errorHighlight");
					} else if (fieldElement.getMetadata().getName() === "sap.m.Switch") {
						$("#" + fieldElement.getId() + "-switch").removeClass("errorHighlight");
					}

				}
			}
			this.getOwnerComponent().getModel("globalModel").setProperty("draftText", "");
			this.getView().byId("validationErrorAnchor").setVisible(false).setText("");
			this._auditTrailRun = "";

		} catch (error) {
			sap.m.MessageToast.show("Error" + error);
		}
	},

	showValidationErrorPopover: function(oEvent) {
		var messageController = sap.ui.core.Fragment.byId("validationPopover", "validationMessageContainer");
		messageController.openBy(oEvent.getSource());
	},
	handlePostValidationError: function(messages) {
		if (!this._validationMessageDialog) {
			this._validationMessageDialog = sap.ui.xmlfragment("validationPopover",
				"STELO_TS03E01.view.validationErrorDialog", this);
			this.getView().addDependent(this._validationMessageDialog);
		}
		var messageController = sap.ui.core.Fragment.byId("validationPopover", "validationMessageContainer");
		var oModelValidationError = new sap.ui.model.json.JSONModel();
		for (var i = 0; i < messages.length; i++) {
			messages[i].type = "Error";
		}
		oModelValidationError.setData(messages);
		messageController.setModel(oModelValidationError);
		var that = this;
		setTimeout(function() {
			messageController.openBy(that.getView().byId("validationErrorAnchor"));
		}, 100);
	},
	//------------------------------------------------------------------------------------------------------------------//
	//End of Form Submission Related Stubs
	//------------------------------------------------------------------------------------------------------------------//

	//------------------------------------------------------------------------------------------------------------------//
	//Start of Audit Trail & XML View Related Stubs
	//------------------------------------------------------------------------------------------------------------------//
	handleAuditTrail: function(oEvent) {
		if (oEvent.getSource().getIcon() === "sap-icon://expand-group") {
			oEvent.getSource().setIcon("sap-icon://collapse-group");
			this.getView().byId("auditTrailPanel").setExpanded(true);
		} else {
			oEvent.getSource().setIcon("sap-icon://expand-group");
			this.getView().byId("auditTrailPanel").setExpanded(false);
		}
		//Set audit trail selected key based on variant
		this.getView().byId("auditTrailSelector").setSelectedKey(this._formVariant);
	},

	addAuditTrailSeparators: function() {
		//Get icon tab elements->count items and insert separators inbetween
		var iconTabElementsLength = this.getView().byId("auditTrailSelector").getItems().length;
		var index = 0;
		var tabSeparator;
		for (var i = 1; i < iconTabElementsLength; i++) {
			tabSeparator = new sap.m.IconTabSeparator({
				icon: "sap-icon://process"
			});
			this.getView().byId("auditTrailSelector").insertItem(tabSeparator, i + index);
			index = index + 1;
		}
	},

	handleXMLViewer: function(oEvent) {

		var formXML = this.indentXMLData(this.ResponseData.EX_XML);
		var f4XML = this.indentXMLData(this.ResponseData.EX_F4_XML);
		var actionXML = this.indentXMLData(this.ResponseData.EX_ACTIONS_XML);
		var logXML = this.indentXMLData(this.ResponseData.EX_EVENT_LOG);

		if (!this._xmlViewer) {
			this._xmlViewer = sap.ui.xmlfragment("xmlViewer", "STELO_TS03E01.view.xmlViewer", this);
			this.getView().addDependent(this._xmlViewer);
		}

		sap.ui.core.Fragment.byId("xmlViewer", "xmlHTML").setContent('<pre class="prettyprint lang-xml">' + jQuery.sap.encodeHTML(formXML) +
			'</pre>');
		sap.ui.core.Fragment.byId("xmlViewer", "xmlF4").setContent('<pre class="prettyprint lang-xml">' + jQuery.sap.encodeHTML(f4XML) +
			'</pre>');
		sap.ui.core.Fragment.byId("xmlViewer", "xmlActions").setContent('<pre class="prettyprint lang-xml">' + jQuery.sap.encodeHTML(actionXML) +
			'</pre>');
		sap.ui.core.Fragment.byId("xmlViewer", "xmlLog").setContent('<pre class="prettyprint lang-xml">' + jQuery.sap.encodeHTML(logXML) +
			'</pre>');

		this._xmlViewer.open();
	},

	handleAuditTrailSelector: function(oEvent) {
		this.resetFields();
		this._auditTrailRun = true;
		this._formVariant = oEvent.getParameter("key");
		this.auditTrailRun();
	},

	auditTrailRun: function() {
		//Data Declarations
		var oView;
		var sURI = '/sap/opu/odata/STELO/APP_SERVER_SRV/';
		var oModelF41 = new sap.ui.model.xml.XMLModel();
		var oModelF42 = new sap.ui.model.xml.XMLModel();
		var oModelF43 = new sap.ui.model.xml.XMLModel();
		var oModelF44 = new sap.ui.model.xml.XMLModel();
		var oModelF45 = new sap.ui.model.xml.XMLModel();
		var oModelAtt = new sap.ui.model.json.JSONModel();
		var oModel1 = new sap.ui.model.odata.ODataModel(sURI, true);
		var that = this;
		oView = this.getView();
		//Setup blank model to handle attachments
		this.getView().setModel(oModelAtt, "attachmentsData");

		// TODO: Help Text

		var sEntity =
			"/AppDataOutSet(IM_DOCUMENT='00000000',IM_ID='" + this._formID + "',IM_ID_VAR='" + this._formVariant +
			"',IM_FTYPE='TS03',IM_CCODE='ACL',IM_FVER='00',IM_FLANG='E',IM_FILLABLE='',IM_CALLER='D')";

		//Common Stub for navigation from different views
		this._oDialog = this.getView().byId("BusyDialog");
		this._oDialog.setText("Prepopulating from server");
		this._oDialog.open();

		//Set opacity to 1
		$('#sap-ui-blocklayer-popup').addClass("blankOut");
		//Read data from backend via Gateway
		oModel1.read(sEntity, null, null, true, function(oData, oResponse) {
			that.ResponseData = oResponse.data;
			oModelF41.setXML(oResponse.data.EX_XML);
			oModelF42.setXML(oResponse.data.EX_F4_XML);
			oModelF43.setXML(oResponse.data.EX_ACTIONS_XML);
			oModelF45.setXML(oResponse.data.EX_AUDIT_TRAIL);

			if (oResponse.data.EX_ATTACHMENTS) { //If Attachments Exists
				oModelF44.setXML(oResponse.data.EX_ATTACHMENTS);
				oView.setModel(oModelF44, "attachmentsModel");
				//Initialise Attachments view
				that.initialiseAttachments();
			}

			oView.setModel(oModelF42, "F4Model");
			that.setupPrepopModel(oView, oModelF41);
			that.buildFieldArray();
			that._oDialog.close();
		});
	},
	handleXMLViewerClose: function(oEvent) {
		this._xmlViewer.close();
	},

	indentXMLData: function(xmlstring) {

		//now lets format the XML and set the indent correctly
		var sFormattedXML = '';
		var reg = /(>)(<)(\/*)/g;
		var sXML = xmlstring.replace(reg, '$1\r\n$2$3');
		var pad = 0;

		jQuery.sap.each(sXML.split('\r\n'), function(index, node) {
			var indent = 0;
			if (node.match(/.+<\/\w[^>]*>$/)) {
				indent = 0;
			} else if (node.match(/^<\/\w/)) {
				if (pad != 0) {
					pad -= 1;
				}
			} else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
				indent = 1;
			} else {
				indent = 0;
			}

			var padding = '';
			for (var i = 0; i < pad; i++) {
				padding += '  ';
			}

			sFormattedXML += padding + node + '\r\n';
			pad += indent;
		});
		return sFormattedXML;
	},

	//------------------------------------------------------------------------------------------------------------------//
	//End of Audit Trail & XML View Related Stubs
	//------------------------------------------------------------------------------------------------------------------//

	//------------------------------------------------------------------------------------------------------------------//
	//Start of Attachment stubs
	//------------------------------------------------------------------------------------------------------------------//
	initialiseAttachments: function() {
		var attachmentsModel = this.getView().getModel("attachmentsModel");
		var noAttachments = attachmentsModel.oData.childNodes[0].childNodes.length;
		var oItem = {};
		var aItems = [];
		var downloadurl, uploadedBy;
		var enableButton = "true";

		//Build attachments object model to prepop uploadcollection control
		for (var i = 0; i < noAttachments; i++) {
			var bufferModel = new sap.ui.model.xml.XMLModel();
			bufferModel.setData(attachmentsModel.oData.childNodes[0].childNodes[i]);
			bufferModel.setXML(bufferModel.getXML());
			downloadurl = "/sap/opu/odata/STELO/APP_SERVER_SRV/AttContentSet('" + bufferModel.getProperty("/CMS_DOC") + "')/$value";
			uploadedBy = bufferModel.getProperty("/UPLOADED_BY");
			//Override for dashboard and history
			if (this._triggeredBy === "Dashboard" || this._triggeredBy === "History") {
				enableButton = "false";
			} else {
				//Disable and hide delete button if the current user is not the uploaded by user
				if (uploadedBy !== this._formOwner) {
					enableButton = "false";
				}
			}
			//Build Attachment object
			oItem = {
				"documentId": jQuery.now().toString(), // generate Id,
				"fileName": bufferModel.getProperty("/DESCRIPTION"),
				"mimeType": bufferModel.getProperty("/MIMETYPE"),
				"thumbnailUrl": "",
				"url": downloadurl,
				"enableDelete": enableButton,
				"visibleDelete": enableButton,
				"visibleEdit": "false",
				"enableEdit": "false",
				"attributes": [{
					"title": "Uploaded By",
					"text": bufferModel.getProperty("/UPLOADED_BY")
				}, {
					"title": "File Size",
					"text": bufferModel.getProperty("/FILESIZE") + " KB"
				}, {
					"title": "CMS Doc",
					"text": bufferModel.getProperty("/CMS_DOC")
				}]
			};
			aItems.push(oItem);
		}
		if (aItems) { //Check if there is any attachments available
			//Set Attachments Model with data
			this.getView().getModel("attachmentsData").setData({
				"items": aItems
			});
			//Update Attachment numbers
			this.getView().byId("attachmentAnchor").setText("Attachments (" + noAttachments + ")");
		}
	},

	formatAttribute: function(sValue) {
		jQuery.sap.require("sap.ui.core.format.FileSizeFormat");
		if (jQuery.isNumeric(sValue)) {
			return sap.ui.core.format.FileSizeFormat.getInstance({
				binaryFilesize: false,
				maxFractionDigits: 1,
				maxIntegerDigits: 3
			}).format(sValue);
		} else {
			return sValue;
		}
	},
	handleAttachments: function(oEvent) {
		if (!this._oAttachmentsDialog) {
			this._oAttachmentsDialog = sap.ui.xmlfragment("attachD",
				"STELO_TS03E01.view.attachmentsDialog", this);
			this.getView().addDependent(this._oAttachmentsDialog);
		}
		//Set Upload URL for upload collection control
		var uploadControl = sap.ui.core.Fragment.byId("attachD", "UploadCollection");
		var cmsdoc = this.getView().getModel("prepopModel").getProperty("/FLM_RETURN").substr(2, 29);
		this._cmsdoc = cmsdoc;
		var uploadURL = "/sap/opu/odata/STELO/APP_SERVER_SRV/ATT_METASet('" + cmsdoc + "')/Attachment";
		uploadControl.setUploadUrl(uploadURL);
		if (this._triggeredBy === "Dashboard" || this._triggeredBy === "History") {
			sap.ui.core.Fragment.byId("attachD", "UploadCollection").setUploadEnabled(false);
		} else {
			sap.ui.core.Fragment.byId("attachD", "UploadCollection").setUploadEnabled(true);
		}
		this._oAttachmentsDialog.open();
	},

	onAttachmentChange: function(oEvent) {
		var oUploadCollection = oEvent.getSource();
		var fileObject = {};
		fileObject = oEvent.getParameter("files")[0];
		this._attachmentsMetadata = fileObject;

		// Header Token
		var oCustomerHeaderToken = new sap.m.UploadCollectionParameter({
			name: "x-csrf-token",
			value: this._csrfToken
		});
		oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
		//Fix to missing slug parameter in the backend
		var uploadURL = "/sap/opu/odata/STELO/APP_SERVER_SRV/AttMetaSet('" + this._cmsdoc + "|" + encodeURIComponent(fileObject.name) +
			"')/Attachment";
		oUploadCollection.setUploadUrl(uploadURL);
	},

	onFileDeleted: function(oEvent) {
		//Call Gateway delete service
		var sURI = '/sap/opu/odata/STELO/APP_SERVER_SRV/';
		var oDeleteModel = new sap.ui.model.odata.ODataModel(sURI, true);
		oDeleteModel.getSecurityToken();
		//oEvent.getParameter("item").mProperties.url;
		var bindingContent = oEvent.getParameter("item").getBindingContext("attachmentsData").getPath;
		var cmsdoc = this.getView().getModel("attachmentsData").getProperty(bindingContent).attributes[2].text;
		var sEntity = "/AttContentSet('" + cmsdoc + "')/$value";
		var oView = this.getView();
		oDeleteModel.remove(sEntity, true, function(oData, oResponse) {
			var oData = oView.getModel("attachmentsData").getData();
			var aItems = jQuery.extend(true, {}, oData).items;
			var sDocumentId = oEvent.getParameter("documentId");
			jQuery.each(aItems, function(index) {
				if (aItems[index] && aItems[index].documentId === sDocumentId) {
					aItems.splice(index, 1);
				}
			});
			oView.getModel("attachmentsData").setData({
				"items": aItems
			});
			var oUploadCollection = sap.ui.core.Fragment.byId("attachD", "UploadCollection");
			oUploadCollection.setNumberOfAttachmentsText("Attachments (" + oUploadCollection.getItems().length + ")");
			oView.byId("attachmentAnchor").setText("Attachments (" + oUploadCollection.getItems().length + ")");

		}, function() {
			sap.m.MessageToast.show("Delete failed");
		});

	},
	closeAttachmentsDialog: function(oEvent) {
		this._oAttachmentsDialog.close();
	},

	onBeforeUploadStarts: function(oEvent) {
		// Header Slug
		var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
			name: "slug",
			value: oEvent.getParameter("fileName")
		});
		oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
		oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
			name: "fileSize",
			value: this._attachmentsMetadata.size
		});
		oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
		//Add Content Type as well if metadata is blank
		if (this._attachmentsMetadata.type === "") {
			oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
				name: "Content-Type",
				value: "application/x-zip-compressed"
			});
			oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
		}

		//Increase upload counter during upload kick off
		this._globalAttUploadCounter = this._globalAttUploadCounter + 1;
	},

	onUploadComplete: function(oEvent) {
		//Decrease upload pending counter on acknowledgement
		this._globalAttUploadCounter = this._globalAttUploadCounter - 1;
		if (oEvent.getParameter("mParameters").status === 201) { //Response created = 201
			var oData = this.getView().getModel("attachmentsData").getData();
			var aItems = [];
			if (oData.items) {
				aItems = oData.items;
			}
			var oItem = {};
			var sUploadedFile = oEvent.getParameter("files")[0].fileName;
			var oResponseModel = new sap.ui.model.xml.XMLModel();
			oResponseModel.setXML(oEvent.getParameter("files")[0].responseRaw);

			var cmsdoc = oResponseModel.getProperty("/m:properties/d:CMS_DOC/");

			//Get file size sent in request header
			var filesize = oResponseModel.getProperty("/m:properties/d:FILE_SIZE/");
			var filestatus = oResponseModel.getProperty("/m:properties/d:ASTATUS/");
			var uploadedby = oResponseModel.getProperty("/m:properties/d:UPLOADED_BY/");
			var mimetype = oResponseModel.getProperty("/m:properties/d:MIME_TYPE/");
			var downloadurl = oResponseModel.getProperty("/m:properties/d:DOWNLOAD_URL/");

			filesize = filesize + " KB";

			if (!sUploadedFile) {
				var aUploadedFile = (oEvent.getParameters().getSource().getProperty("value")).split(/\" "/);
				sUploadedFile = aUploadedFile[0];
			}
			oItem = {
				"documentId": jQuery.now().toString(), // generate Id,
				"fileName": sUploadedFile,
				"mimeType": mimetype,
				"thumbnailUrl": "",
				"visibleEdit": "false",
				"enableEdit": "false",
				"url": downloadurl,
				"attributes": [{
					"title": "Uploaded By",
					"text": uploadedby
				}, {
					"title": "File Size",
					"text": filesize
				}, {
					"title": "CMS Doc",
					"text": cmsdoc
				}]
			};
			if (aItems.length === 0) {
				aItems.push(oItem);
			} else {
				aItems.unshift(oItem);
			}

			this.getView().getModel("attachmentsData").setData({
				"items": aItems
			});

			var oUploadCollection = sap.ui.core.Fragment.byId("attachD", "UploadCollection");
			oUploadCollection.setNumberOfAttachmentsText("Attachments (" + oUploadCollection.getItems().length + ")");
			this.getView().byId("attachmentAnchor").setText("Attachments (" + oUploadCollection.getItems().length + ")");
		} else if (oEvent.getParameter("mParameters").status === 0) {
			sap.m.MessageToast.show("Upload Terminated");
		} else {
			sap.m.MessageToast.show("Upload Error - Response Code::" + oEvent.getParameter("mParameters").status);
			oData = this.getView().getModel("attachmentsData").getData();
			aItems = [];
			if (oData.items) {
				aItems = oData.items;
			}
			//Enhancement to flush failed item from upload list
			aItems.unshift("blank object");
			this.getView().getModel("attachmentsData").setData({
				"items": aItems
			});
			aItems.shift();
			this.getView().getModel("attachmentsData").setData({
				"items": aItems
			});
			oUploadCollection = sap.ui.core.Fragment.byId("attachD", "UploadCollection");
			oUploadCollection.setNumberOfAttachmentsText("Attachments (" + oUploadCollection.getItems().length + ")");
			this.getView().byId("attachmentAnchor").setText("Attachments (" + oUploadCollection.getItems().length + ")");
		}
	},
	handleCloseButton: function() {
		this._oAttachmentsDialog.close();
	},

	
	onAddPress: function() {
		
		// console.log(this.getView().byId("DT_DATE").getMetadata().getName());
		// console.log(this.getView().byId("DT_DATE").data("required"));

		var d = new Date();
		var n = d.getSeconds();
		var newXMLNode =
			'<SF_DATE_TIME_DETAILS label="Date and Time Details" visible="true" maxoccurs="0099"><DD_WAGE_TYPE label="Wage Type" visible="true" enabled="true" required="true"></DD_WAGE_TYPE><DT_DATE label="Date" visible="true" enabled="true" required="true"></DT_DATE><NUM_TOTAL_HOURS label="Total Hours" visible="true" enabled="true" required="true">' + n + '</NUM_TOTAL_HOURS><TM_END_TIME_1 label="End Time" visible="true" enabled="true" required="false"/><TM_END_TIME_2 label="End Time" visible="true" enabled="true" required="false"/><TM_END_TIME_3 label="End Time" visible="true" enabled="true" required="false"/><TM_START_TIME_1 label="Start Time" visible="true" enabled="true" required="false"/><TM_START_TIME_2 label="Start Time" visible="true" enabled="true" required="false"/><TM_START_TIME_3 label="Start Time" visible="true" enabled="true" required="false"/><TM_START_TIME_4 label="Start Time" visible="true" enabled="true" required="false"/></SF_DATE_TIME_DETAILS>';
				var oModel = this.getView().getModel("prepopModel");
		var oXML = oModel.getObject("/SF_TIMESHEET_DETAILS");
		// console.log(oXML);
		// console.log(oModel.getProperty("/SF_TIMESHEET_DETAILS/0/SF_DATE_TIME_DETAILS/DT_DATE/@required"));
		$(oXML).append(newXMLNode);
		oModel.refresh();
		// this.getView().byId("SF_DATE_TIME_DETAILS").getBinding("items").refresh();
		// console.log(oXML);

	},

	deleteRow: function(oEvent) {
		// console.log(oEvent.getSource());
		// console.log(oEvent.getSource().getBindingContext("prepopModel"));
		// console.log(oEvent.getSource().getBindingContext("prepopModel").getObject());
		var sPath = oEvent.getSource().getBindingContext("prepopModel").getPath();
		// console.log(sPath);
		// console.log(this.getView().byId("lineItemsListXML").getModel().oData.childNodes[0]);
		var oModel = this.getView().getModel("prepopModel");
		var oXML = oModel.getObject(sPath);
		// console.log(oXML);
		$(oXML).remove();

		oModel.refresh();
	},

	createEntry: function() {
		return this.oModel.getData();
	},

	getDataFromSelectedRows: function(table, path) {
			return table.getSelectedIndices().map(function(index) {
				return table.getContextByIndex(index).getProperty(path);
			});
		}
		//------------------------------------------------------------------------------------------------------------------//
		//End of Attachment stubs
		//------------------------------------------------------------------------------------------------------------------//
});