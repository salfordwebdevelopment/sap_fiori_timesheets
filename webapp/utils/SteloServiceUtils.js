sap.ui.define([
	'sap/m/Dialog',
	'sap/m/MessageToast',
	"STELO_TS03E01/utils/TimeoutUtils"
], function (Dialog, MessageToast, TimeoutUtils) {
	"use strict";
	return {
		DEFAULT_MODEL_SIZE: 5000,
		DEFAULT_COUNT_MODE: sap.ui.model.odata.CountMode.None,

		getList: function (oContext, filter, sModelName, sEntity, sURL, sURLParameters, bForceRefresh, bComponentLevel, bDeferred) {
			var that = oContext;
			var deferredPromise = $.Deferred();
			if (bComponentLevel) {
				var sModel = oContext.getOwnerComponent().getModel(sModelName);
			} else {
				sModel = oContext.getView().getModel(sModelName);
			}

			if (!sModel || bForceRefresh) { //Set model to owner component
				var listModel = new sap.ui.model.json.JSONModel();
				listModel.setSizeLimit(this.DEFAULT_MODEL_SIZE);
				if (bComponentLevel) {
					oContext.getOwnerComponent().setModel(listModel, sModelName);
				} else {
					oContext.getView().setModel(listModel, sModelName);
				}

				//Get backend data
				var oDataModel = new sap.ui.model.odata.v2.ODataModel(sURL, false);
				var sPath = sEntity;

				oDataModel.read(sEntity, {
					async: true,
					filters: filter,
					urlParameters: sURLParameters,
					success: function (oData, oResponse) {
						TimeoutUtils.onResetTimer(oContext);
						var aResults = oData.results;
						listModel.setData(aResults);
						listModel.refresh(true);
						//resolve promise if the caller is expecting a promise object
						deferredPromise.resolve(aResults);
					},
					error: function (oError) {
						//Handle error
						//Return reject promise if the caller is expecting a promise object
						deferredPromise.reject(oError);
					}
				});
				return deferredPromise.promise();
			} else {
				deferredPromise.resolve(sModel.getData());
				return deferredPromise.promise();
			}
		},

		createEntry: function (oContext, data, sEntity, sURL) {
			var deferredPromise = $.Deferred();
			var oDataModel = new sap.ui.model.odata.v2.ODataModel(sURL);
			oDataModel.create(sEntity, data, {
				success: function (oData, oResponse) {
					TimeoutUtils.onResetTimer(oContext);
					deferredPromise.resolve(oResponse);
				},
				error: function (oError) {
					deferredPromise.reject(oError);
				}
			});
			return deferredPromise.promise();
		},

		updateEntry: function (oContext, data, sEntity, sURL) {
			var deferredPromise = $.Deferred();
			var oDataModel = new sap.ui.model.odata.v2.ODataModel(sURL);
			oDataModel.update(sEntity, data, {
				success: function (oData, oResponse) {
					TimeoutUtils.onResetTimer(oContext);
					deferredPromise.resolve(oResponse);
				},
				error: function (oError) {
					deferredPromise.reject(oError);
				}
			});
			return deferredPromise.promise();
		},

		deleteEntry: function (oContext, sEntity, sService) {
			var deferredPromise = $.Deferred();
			var sURL = oContext.getOwnerComponent().getMetadata().getManifestEntry("sap.app").dataSources[sService].uri;
			var oDataModel = new sap.ui.model.odata.v2.ODataModel(sURL);
			oDataModel.remove(sEntity, {
				success: function (oData, oResponse) {
					TimeoutUtils.onResetTimer(oContext);
					deferredPromise.resolve(oResponse);
				},
				error: function (oError) {
					deferredPromise.reject(oError);
				}
			});
			return deferredPromise.promise();
		}

	};
});