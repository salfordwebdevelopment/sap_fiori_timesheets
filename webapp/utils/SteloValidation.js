sap.ui.define([
	'sap/m/MessageToast'
], function (messageToast) {
	"use strict";

	return {
		validateGroup: function (context, group) {
			var controls = context.getView().getControlsByFieldGroupId(group);
			var controlValue;
			var errCounter = 0;
			var validationResult = {
				"Status": true,
				"Errors": 0
			};
			for (var i = 0; i < controls.length; i++) {
				var controlType = controls[i].getMetadata().getName();
				switch (controlType) {
				case "sap.m.Input":
				case "sap.m.MaskInput":
				case "sap.m.DatePicker":
				case "sap.m.TimePicker":
					//Check if value exists
					controlValue = controls[i].getValue();

					if (group === "ccValidation") {
						if (this.validateField(context, controls[i]) === false) {
							errCounter++;
						}
					} else { //otherwise we're checking for mandatory fields
						if (controlValue === "") {
							controls[i].setValueState("Error");
							controls[i].setValueStateText("Mandatory field cannot be left empty");
							controls[i].setShowValueStateMessage(true);
							errCounter++;
						} else { //Reset highlight
							controls[i].setValueState("None");
							controls[i].setValueStateText("");
							controls[i].setShowValueStateMessage(false);
						}
					}
					break;
				case "sap.m.Select":
					controlValue = controls[i].getSelectedKey();
					if (controlValue === "") {
						controls[i].addStyleClass("errorHighlight");
						errCounter++;
					} else {
						controls[i].removeStyleClass("errorHighlight");
					}
					break;
				case "sap.m.CheckBox":
					break;
				case "sap.m.Switch":
					break;
				case "sap.m.RadioButton":
					break;

				}
			}
			if (errCounter > 0) {
				return false; //Validation error failure
			} else {
				return true; //Validation successfully passed
			}
		},

		/*		validateRequiredMess: function() {
					sap.m.MessageToast.show("Required fields", {
						my: "center center", // default
						at: "center center", // default
						of: window // default
					});
				}, */

		validateReset: function (context, group) {
			var controls = context.getView().getControlsByFieldGroupId(group);
			for (var i = 0; i < controls.length; i++) {
				var controlType = controls[i].getMetadata().getName();
				switch (controlType) {
				case "sap.m.Input":
				case "sap.m.MaskInput":
				case "sap.m.DatePicker":
				case "sap.m.TimePicker":
					//Check if value exists
					controls[i].setValueState("None");
					controls[i].setValueStateText("");
					controls[i].setShowValueStateMessage(false);
					break;
				case "sap.m.Select":
					controls[i].removeStyleClass("errorHighlight");
					break;
				case "sap.m.CheckBox":
					break;
				case "sap.m.Switch":
					break;
				case "sap.m.RadioButton":
					break;

				}
			}
		},

		validateField: function (context, field, showToast) {

			var errorMsg = this.validateValue(context, field.data("validation"), field.getValue());
			if (errorMsg !== "") {
				field.setShowValueStateMessage(true);
				field.setValueStateText(errorMsg);
				field.setValueState("Error");
				if (showToast) {
					this.showErrorMsg(errorMsg);
				}
				return false;
			} else {
				field.setShowValueStateMessage(false);
				field.setValueStateText("");
				field.setValueState("None");
				return true;
			}
		},

		showErrorMsg: function (errorMsg) {
			sap.m.MessageToast.show(errorMsg, {
				my: "center center", // default
				at: "center center", // default
				of: window // default
			});
		},

		validateValue: function (context, validationType, value) {
			var errorMessage = "";
			var fieldTest;
			var pattern;

			if (value === "") {
				errorMessage = "";
				return errorMessage;
			}

			switch (validationType) {
			case "checkEmail":
				pattern = /(\w+)\@(\w+)\.[a-zA-Z]/g;
				fieldTest = pattern.test(value); // returns a boolean
				if (fieldTest === false) {
					errorMessage = context.getOwnerComponent().getModel("i18n").getProperty("CC_EmailVal");
				} else {
					errorMessage = "";
				}
				break;
			case "checkZip":
				pattern = /^\d{5}(?:[-\s]\d{4})?$/;
				fieldTest = pattern.test(value); // returns a boolean
				if (fieldTest === false) {
					errorMessage = context.getOwnerComponent().getModel("i18n").getProperty("CC_ZipVal");
				} else {
					errorMessage = "";
				}
				break;
			case "checkNumeric":
				pattern = /^[0-9.]+$/;
				fieldTest = pattern.test(value); // returns a boolean
				if (fieldTest === false) {
					errorMessage = context.getOwnerComponent().getModel("i18n").getProperty("CC_NumericVal");
				} else {
					errorMessage = "";
				}
				break;
			case "checkPhone":
				pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
				fieldTest = pattern.test(value); // returns a boolean
				if (fieldTest === false) {
					errorMessage = context.getOwnerComponent().getModel("i18n").getProperty("CC_PhoneVal");
				} else {
					errorMessage = "";
				}
				break;
			case "checkAccNum":

				if (value.indexOf("_") !== -1) {
					errorMessage = context.getOwnerComponent().getModel("i18n").getProperty("Acc_AccNumVal");
				} else {
					errorMessage = "";
				}
				break;
			}
			return errorMessage;

		}
	};
});