sap.ui.define([
	'sap/m/Dialog',
	'sap/m/MessageToast'
], function (Dialog, MessageToast) {
	"use strict";
	return {
		iTimeout: 1700000,
		iWarningTimeout: 30000,
		bStarted: false,
		oNotification: {
			Variables: {
				OriginalTitle: document.title,
				OriginalImg: $("link[rel='shortcut icon']").attr("href"),
				Interval: null
			},
			On: function (sNotification, sNewIcon, iSpeed) {
				var that = this;
				that.Variables.Interval = setInterval(function () {
					if (document.title === that.Variables.OriginalTitle) {
						document.title = sNotification;
						$("link[rel='shortcut icon']").attr("href", sNewIcon);
					} else {
						document.title = that.Variables.OriginalTitle;
						$("link[rel='shortcut icon']").attr("href", that.Variables.OriginalImg);
					}
				}, (iSpeed) ? iSpeed : 1000);
			},
			Off: function () {
				clearInterval(this.Variables.Interval);
				document.title = this.Variables.OriginalTitle;
				$("link[rel='shortcut icon']").attr("href", this.Variables.OriginalImg);
			}
		},
		onStartTimer: function (oContext) {
			this._oContext = oContext;
			if (this._oTimer) {
				clearTimeout(this._oTimer);
			}
			this._oTimer = setTimeout(this._showRunningOutOfTime.bind(this), this.iTimeout);
			this.bStarted = true;
		},
		onResetTimer: function (oContext) {
			clearTimeout(this._oTimer);
			if (this.bStarted) {
				this.onStartTimer(oContext);
			}
		},
		_startLogoutTimer: function () {
			if (this._oTimer) {
				clearTimeout(this._oTimer);
			}
			this._oTimer = setTimeout(this._logoutUser.bind(this), this.iWarningTimeout);
		},
		_logoutUser: function () {
			sap.m.URLHelper.redirect("logout.html", false);
		},
		_showRunningOutOfTime: function () {
			var that = this;
			var dialog = new Dialog("timeout", {
				title: that._oContext.getOwnerComponent().getModel("i18n").getResourceBundle().getText("TO_Title"),
				type: 'Message',
				content: [
					new sap.m.VBox({
						fitContainer: true,
						alignItems: sap.m.FlexAlignItems.Start,
						justifyContent: sap.m.FlexJustifyContent.SpaceBetween,
						items: [
							new sap.m.Text({
								text: that._oContext.getOwnerComponent().getModel("i18n").getResourceBundle().getText("TO_30s")
							})
						]
					})
				],
				afterClose: function () {
					dialog.destroy();
				}
			});
			var beginBtn = new sap.m.Button({
				text: that._oContext.getOwnerComponent().getModel("i18n").getResourceBundle().getText("TO_Continue"),
				press: function () {
					//SpinnerUtils.startSpinner(that._oContext);
					// var oSAPPromise = RegistrationUtils.getSAPUserData(that._oContext, that._oContext.getOwnerComponent().getModel("userapi").getProperty(
					// 	"/name"));
					// oSAPPromise.then(function(oSAPData) {
					// 	//SpinnerUtils.stopSpinner(that._oContext);
					// 	that.onResetTimer(that._oContext);
					// 	that.oNotification.Off();
					// 	dialog.close();
					// }, function(oError) {
					// 	//SpinnerUtils.stopSpinner(that._oContext);
					// 	that.oNotification.Off();
					// 	dialog.close();
					// 	that._logoutUser();
					// });
				}
			});
			beginBtn.addStyleClass("btn btn-primary");
			var endBtn = new sap.m.Button({
				text: that._oContext.getOwnerComponent().getModel("i18n").getResourceBundle().getText("TO_Logout"),
				press: function () {
					that._logoutUser();
					dialog.close();
				}
			});
			endBtn.addStyleClass("btn btn-secondary");
			dialog.setBeginButton(beginBtn);
			dialog.setEndButton(endBtn);
			dialog.open();
			that._startLogoutTimer();
			var sIPath = jQuery.sap.getModulePath("com.thermofisher.smartistcentral") + "/images/Warning.png";
			that.oNotification.On(that._oContext.getOwnerComponent().getModel("i18n").getResourceBundle().getText("TO_Title"), sIPath);
		}
	};
});